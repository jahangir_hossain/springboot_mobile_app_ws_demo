package com.linkstaff.app.ws.ui.model.response;

public enum RequestOperationStatus {
	ERROR, SUCCESS
}
