package com.linkstaff.app.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linkstaff.app.ws.exceptions.UserServiceException;
import com.linkstaff.app.ws.service.UserService;
import com.linkstaff.app.ws.shared.dto.UserDto;
import com.linkstaff.app.ws.ui.model.request.UserDetailsRequestModel;
import com.linkstaff.app.ws.ui.model.response.ErrorMessages;
import com.linkstaff.app.ws.ui.model.response.OperationStatusModel;
import com.linkstaff.app.ws.ui.model.response.RequestOperationName;
import com.linkstaff.app.ws.ui.model.response.RequestOperationStatus;
import com.linkstaff.app.ws.ui.model.response.UserRes;

@RestController
@RequestMapping("users") // http:localhost:8080/users
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public List<UserRes> getUser(@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "25") int limit) {

		List<UserRes> returnValue = new ArrayList<>();
		ModelMapper modelMapper = new ModelMapper();
		
		List<UserDto> userList = userService.getUsers(page, limit);
		
		for(UserDto user: userList) {
			returnValue.add(modelMapper.map(user, UserRes.class));
			
		}

		return returnValue;
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserRes getUser(@PathVariable String id) {

		UserDto userDto = userService.getUserById(id);
		
		return new ModelMapper().map(userDto, UserRes.class);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserRes createUser(@RequestBody UserDetailsRequestModel userDetails) throws Exception {

		ModelMapper modelMapper = new ModelMapper();
		UserDto userDto = modelMapper.map(userDetails, UserDto.class);

		UserDto createdUser = userService.createUser(userDto);
		
		return modelMapper.map(createdUser, UserRes.class);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public UserRes updateUser(@PathVariable String id, @RequestBody UserDetailsRequestModel userDetails) {

		UserRes returnValue = new UserRes();

		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userDetails, userDto);

		UserDto updatedUser = userService.updateUser(id, userDto);
		BeanUtils.copyProperties(updatedUser, returnValue);

		return returnValue;
	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteUser(@PathVariable String id) {

		OperationStatusModel returnValue = new OperationStatusModel();

		returnValue.setOperationName(RequestOperationName.DELETER.name());

		userService.deleteUser(id);

		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		return returnValue;
	}

}
