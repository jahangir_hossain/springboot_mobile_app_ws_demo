package com.linkstaff.app.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Type;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linkstaff.app.ws.service.AddressService;
import com.linkstaff.app.ws.service.UserService;
import com.linkstaff.app.ws.shared.dto.AddressDto;
import com.linkstaff.app.ws.shared.dto.UserDto;
import com.linkstaff.app.ws.ui.model.response.AddressRes;
import com.linkstaff.app.ws.ui.model.response.UserRes;

@RestController
@RequestMapping("users/{userId}/addresses") // http:localhost:8080/users/fdasfdsa/addresses
public class AddressController {

	@Autowired
	AddressService addressService;

	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public Resources<AddressRes> getAddresses(@PathVariable String userId) {

		List<AddressRes> addressListResModel = new ArrayList<>();

		List<AddressDto> addressList = addressService.getUserAddress(userId);

		if (addressList != null && !addressList.isEmpty()) {
			Type listType = new TypeToken<List<AddressRes>>() {
			}.getType();
			addressListResModel = new ModelMapper().map(addressList, listType);
			for(AddressRes address:  addressListResModel) {
				Link addressLink = linkTo(methodOn(AddressController.class).getAddress(userId, address.getAddressId())).withSelfRel();
				address.add(addressLink);
				
				Link userLink = linkTo(methodOn(UserController.class).getUser(userId)).withRel("user");
				address.add(userLink);
			}
		}

		return new Resources<>(addressListResModel);
	}

	@GetMapping(path = "/{addressId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public Resource<AddressRes> getAddress(@PathVariable String userId, @PathVariable String addressId) {
		AddressDto addressDto = addressService.getAddress(addressId);
		
		AddressRes addressResModel = new ModelMapper().map(addressDto, AddressRes.class);
		
		Link addressLink = linkTo(methodOn(AddressController.class).getAddress(userId,addressId)).withSelfRel();
		Link addressesLink = linkTo(methodOn(AddressController.class).getAddresses(userId)).withRel("addresses");
		Link userLink = linkTo(methodOn(UserController.class).getUser(userId)).withRel("user");
		
		addressResModel.add(addressLink);
		addressResModel.add(userLink);
		addressResModel.add(addressesLink);
		
		return new Resource<>(addressResModel);
	}

}
