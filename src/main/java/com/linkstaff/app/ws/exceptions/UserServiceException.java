package com.linkstaff.app.ws.exceptions;

public class UserServiceException extends RuntimeException{

	private static final long serialVersionUID = -6700591723238484633L;

	public UserServiceException(String message) {
		super(message);
	}
	
	
}
