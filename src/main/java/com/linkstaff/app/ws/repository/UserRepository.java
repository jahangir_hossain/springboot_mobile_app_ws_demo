package com.linkstaff.app.ws.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.linkstaff.app.ws.io.entity.UserEntity;

public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {
	UserEntity findByEmail(String email); 
	UserEntity findByUserId(String userId); 
}
