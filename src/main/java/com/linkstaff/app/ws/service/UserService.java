package com.linkstaff.app.ws.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.linkstaff.app.ws.shared.dto.UserDto;

public interface UserService  extends UserDetailsService{

	UserDto createUser(UserDto userDto);
	UserDto getUser(String email);
	UserDto getUserById(String userId);
	UserDto updateUser(String userId, UserDto userDetails);
	void deleteUser(String userId);
	List<UserDto> getUsers(int page, int limit);

}
