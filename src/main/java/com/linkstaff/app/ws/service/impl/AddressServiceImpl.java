package com.linkstaff.app.ws.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.linkstaff.app.ws.io.entity.AddressEntity;
import com.linkstaff.app.ws.io.entity.UserEntity;
import com.linkstaff.app.ws.repository.AddressRepository;
import com.linkstaff.app.ws.repository.UserRepository;
import com.linkstaff.app.ws.service.AddressService;
import com.linkstaff.app.ws.service.UserService;
import com.linkstaff.app.ws.shared.dto.AddressDto;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	AddressRepository addressRepositoty;

	@Override
	public List<AddressDto> getUserAddress(String userId) {
		List<AddressDto> returnValue = new ArrayList<AddressDto>();
		ModelMapper modelMapper = new ModelMapper();

		UserEntity userEntity = userRepository.findByUserId(userId);

		if (userEntity == null)
			throw new UsernameNotFoundException(userId);

		Iterable<AddressEntity> addresses = addressRepositoty.findAllByUserDetails(userEntity);

		for (AddressEntity address : addresses) {
			returnValue.add(modelMapper.map(address, AddressDto.class));

		}

		return returnValue;
	}

	@Override
	public AddressDto getAddress(String addressId) {

		AddressEntity addressEntity = addressRepositoty.findByAddressId(addressId);

		if (addressEntity == null)
			throw new UsernameNotFoundException(addressId);
		return new ModelMapper().map(addressEntity, AddressDto.class);
	}

}
