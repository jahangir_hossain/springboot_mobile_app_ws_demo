package com.linkstaff.app.ws.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.linkstaff.app.ws.exceptions.UserServiceException;
import com.linkstaff.app.ws.io.entity.UserEntity;
import com.linkstaff.app.ws.repository.UserRepository;
import com.linkstaff.app.ws.service.UserService;
import com.linkstaff.app.ws.shared.Utils;
import com.linkstaff.app.ws.shared.dto.AddressDto;
import com.linkstaff.app.ws.shared.dto.UserDto;
import com.linkstaff.app.ws.ui.model.response.ErrorMessages;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepositoy;

	@Autowired
	Utils utils;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public UserDto createUser(UserDto userDto) {
		
		ModelMapper modelMapper = new ModelMapper();

		if (userRepositoy.findByEmail(userDto.getEmail()) != null)
			throw new RuntimeException("Record already exists");
		
		for(AddressDto address: userDto.getAddresses()) {
			address.setUserDetails(userDto);
			address.setAddressId(utils.generateAddressId(30));
		}

		UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);

		userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
		userEntity.setUserId(utils.generateUserId(30));

		UserEntity storedUserDetails = userRepositoy.save(userEntity);

		UserDto returnValue = modelMapper.map(storedUserDetails, UserDto.class);

		return returnValue;
	}

	@Override
	public UserDto getUser(String email) {

		UserEntity userEntity = userRepositoy.findByEmail(email);
		if (userEntity == null)
			throw new UsernameNotFoundException(email);

		UserDto returnValue = new UserDto();
		BeanUtils.copyProperties(userEntity, returnValue);

		return returnValue;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity userEntity = userRepositoy.findByEmail(email);

		if (userEntity == null)
			throw new UsernameNotFoundException(email);

		return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), new ArrayList<>());
	}

	@Override
	public UserDto getUserById(String userId) {

		UserEntity userEntity = userRepositoy.findByUserId(userId);
		if (userEntity == null)
			throw new UsernameNotFoundException(userId);

		UserDto returnValue = new UserDto();
		BeanUtils.copyProperties(userEntity, returnValue);

		return returnValue;
	}

	@Override
	public UserDto updateUser(String userId, UserDto userDetails) {

		UserEntity userEntity = userRepositoy.findByUserId(userId);
		if (userEntity == null)
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		userEntity.setFirstName(userDetails.getFirstName());
		userEntity.setLastName(userDetails.getLastName());

		UserEntity updatedUserDetails = userRepositoy.save(userEntity);

		UserDto returnValue = new UserDto();
		BeanUtils.copyProperties(updatedUserDetails, returnValue);

		return returnValue;
	}

	@Override
	public void deleteUser(String userId) {
		
		UserEntity userEntity = userRepositoy.findByUserId(userId);
		if (userEntity == null)
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
		
		userRepositoy.delete(userEntity);
	}
	
	@Override
	public List<UserDto> getUsers(int page, int limit) {
		
		if(page> 0) page-= 1;
		
		List<UserDto> returnValue = new ArrayList<>();
		
		Pageable pageable = PageRequest.of(page, limit); 
		
		Page<UserEntity> usersPage =  userRepositoy.findAll(pageable);
		List<UserEntity> users = usersPage.getContent();
		
		for(UserEntity user: users) {
			UserDto userDto = new UserDto();
			BeanUtils.copyProperties(user, userDto);
			returnValue.add(userDto);
		}
		
		
		return returnValue;
	}

}
