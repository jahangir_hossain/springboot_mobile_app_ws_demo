package com.linkstaff.app.ws.service;

import java.util.List;

import com.linkstaff.app.ws.shared.dto.AddressDto;

public interface AddressService {
	List<AddressDto> getUserAddress(String userId);

	AddressDto getAddress(String addressId);

}
